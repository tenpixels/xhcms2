<?php
/**
 * ============================================================================
 * * COPYRIGHT 2016-2019 xhadmin.com , and all rights reserved.
 * * WEBSITE: http://www.xhadmin.com;
 * ----------------------------------------------------------------------------
 * This is not a free software!You have not used for commercial purposes in the
 * premise of the program code to modify and use; and publication does not allow
 * any form of code for any purpose.
 * ============================================================================
 * Author: 寒塘冷月 QQ：274363574
 */

namespace app\index\controller;
use app\index\service\FormExtendService;


class Form extends Base
{
	
	//表单提交页面
	public function index(){
		if ($this->request->isPost()){
			$formData = $this->request->post();
			if(empty($formData['form_id'])){
				$this->error('模型ID不能为空');
			}
			try {
				$res = FormExtendService::saveData($formData);
			} catch (\Exception $e) {
				$this->error($e->getMessage());
			}
			$this->success('提交成功');
		}
	}
}
