<?php
/**
 * ============================================================================
 * * COPYRIGHT 2016-2019 xhadmin.com , and all rights reserved.
 * * WEBSITE: http://www.xhadmin.com;
 * ----------------------------------------------------------------------------
 * This is not a free software!You have not used for commercial purposes in the
 * premise of the program code to modify and use; and publication does not allow
 * any form of code for any purpose.
 * ============================================================================
 * Author: 寒塘冷月 QQ：274363574
 */

namespace app\index\controller;
use app\index\service\BaseService;

class Index extends Base
{
	
	//首页
	public function index(){
		$this->view->assign('media', baseService::getMedia());  //网站关键词描述信息
		$this->view->assign('pid',0);
		$default_themes = config('xhadmin.default_themes') ? config('xhadmin.default_themes') : 'index';
		return view($default_themes.'/index');
	}
	
	
	
}
