<?php
/**
 * ============================================================================
 * * COPYRIGHT 2016-2019 xhadmin.com , and all rights reserved.
 * * WEBSITE: http://www.xhadmin.com;
 * ----------------------------------------------------------------------------
 * This is not a free software!You have not used for commercial purposes in the
 * premise of the program code to modify and use; and publication does not allow
 * any form of code for any purpose.
 * ============================================================================
 * Author: 寒塘冷月 QQ：274363574
 */

namespace app\admin\validate;
use think\validate;

class User extends validate {


	protected $rule = [
		'name'=>['require'],
		'user'=>['require'],
		'pwd'=>['require','regex'=>'/^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,20}$/'],
	];

	protected $message = [
		'name.require'=>'真实姓名不能为空',
		'user.require'=>'用户名不能为空',
		'pwd.require'=>'密码不能为空',
		'pwd.regex'=>'6-21位数字字母组合',
	];

	protected $scene  = [
		'add'=>['name','user','pwd'],
		'update'=>['name','user'],
		'updatePassword'=>['pwd'],
	];



}

