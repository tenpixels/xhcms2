<?php
/**
 * 数据表中间件
 * ============================================================================
 * * COPYRIGHT 2016-2019 xhadmin.com , and all rights reserved.
 * * WEBSITE: http://www.xhadmin.com;
 * ----------------------------------------------------------------------------
 * This is not a free software!You have not used for commercial purposes in the
 * premise of the program code to modify and use; and publication does not allow
 * any form of code for any purpose.
 * ============================================================================
 * Author: 寒塘冷月 QQ：274363574
 */

namespace app\admin\middleware;

class SetTable
{
	
    public function handle($request, \Closure $next)
    {	
		$data = $request->param();
		try{
			//创建数据表
			$sql=" CREATE TABLE IF NOT EXISTS `".config('database.connections.mysql.prefix').config('my.create_table_pre').$data['table_name']."` ( ";
			$sql .= '
				`data_id` int(10) NOT NULL AUTO_INCREMENT ,
				PRIMARY KEY (`data_id`)
				) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;
			';
			db()->execute($sql);
			
			if($data['type'] == 1){
				$propertyField = \app\admin\service\FieldSetService::propertyField();
				$property = $propertyField[2];
				$sql = "ALTER TABLE ".config('database.connections.mysql.prefix').config('my.create_table_pre')."{$data['table_name']} ADD content_id {$property['name']}({$property['maxlen']}{$property['decimal']}) DEFAULT NULL";
			}
			db()->execute($sql); 
		}catch(\Exception $e){
			abort(config('my.error_log_code'),$e->getMessage());
		}
		return $next($request);
    }
}